#!/usr/bin/env bash
########################################################################
#created by: silent-mobius
#purpose: automation laptop install with bash and ansible
#date: 01/03/2021
#version: 0.4.9
#######################################################################

msg_escalate_permissions="get root"


#####
# Functions /\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
#####

deco(){
l="##########################"
printf "\n$l\n## %s\n$l\n" "$@"
}

system_type(){
    system=$(cat /etc/*-release|)
}

setup_ansible_env(){
    mkdir .ansible_env
    if [[ $(which pipenv) ]] ;then  
        pipenv .ansible_env
        cd .ansible_env
        pipenv shell
    else
        if [[ $(which pip3) ]];then 
            $_installer install -y python3-pip3
}

verify_dependencies(){
    true
}

start_provision(){
    true
}
###########
# Main     - _- _- _- _- _- _- _- _- _- _- _- _- _- _- _- _- _- _- _- _- _- _- _
###########

if [[ $EUDI != 0 ]];then
    deco "$msg_escalate_permissions"
else
    setup_ansible_env
    verify_dependencies
    start_provision
fi
