# Ansible Env Setup

## what do we need

- ### Physical nodes

- ### Virtual Nodes

## Architecture  description


## Inventory


## Ansible config files

### Static Inventory

- #### [defaults]  
<!-- things that are usually configured as defaults in ansible -->
  -
- #### [privilege_escalation]
<!-- how privilege_escalation is done in ansible config -->
  -

- #### testing the configured files
<!-- adding ansible all -m command  -a 'useradd alex' -->

### Dynamic Inventory

- #### what is it needed for, if you already have `Static Inventory` ?
