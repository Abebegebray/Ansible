# Automating Development and IT Environment With Ansible

- Intro:
  - History
  - Configuration Management
  - Environment Architecture

- Setup:
  - Environment Discussion
  - Install and Setup

- Ansible Fundaments:

  - Initial Config
  - Ansible Commands
  - Ansible Modules
  - Ansible Plugins
  - Ansible Playbooks

- Ansible Playbooks:
  - Yaml Basics
    - Structure Of Ansible Playbook
    - Ansible Modules In Playbooks
  - Storing And Passing Information With In Playbook
  - Templates
  - Looping Through Variables And Lists
  - Modules For Various Applications
  - Handlers Of Services

- Ansible Roles:
  - Converting To Roles
  - Tasks and Handles
  - Files and Templates
  - Including Other Playbooks
  - Encrypting Things In Ansible

- Ansible Advance Execution
  - Fact Gathering
  - Limits
  - Tags
  - Idempotence:
    - changed_when
    - failed_when

- Troubleshooting Testing And Validation:
  - Troubleshooting issues
  - Jumping To Specific
  - Debugging
---
© All Right reserved to Alex M. Schapelle of VaioLabs inc.
